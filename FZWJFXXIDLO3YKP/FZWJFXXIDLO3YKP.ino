#include <PWMServo.h>

#include <SoftwareSerial.h>

#include <Arduino.h>

SoftwareSerial mySerial(5, 6);

PWMServo myservo;

String inString = "";

unsigned long startTime;

unsigned long otherTime;

int prev = 150;

float vPow = 5;

float r1 = 47000;

float r2 = 10000;

int voltcheck = 25;

double b = 85; //Braking

double n = 90; //Neutral

double m = 120; //Accelerate

void setup() {

	pinMode(13, OUTPUT);

	myservo.attach(9);

	mySerial.begin(9600);

	digitalWrite(13, LOW);

}

void loop() {

	if (mySerial.available() > 0) {

		int inChar = mySerial.read();

		if (isDigit(inChar)) {

			inString += (char)inChar;
     
     int x = inString.toInt();

      if (x <= 180){

        myservo.write(x);
        Serial.println(x);
		}
      inString = "";
		}

		if (inChar == 'm') {

			digitalWrite(13, HIGH);

			//Serial.println("ALIVE");

			startTime = millis();

		}

	}

	if ((millis() - startTime) > 210) {

		//digitalWrite(13, LOW);

		myservo.write(90);

		delay(30);

	}

}
