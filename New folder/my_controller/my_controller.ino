#include <PWMServo.h>
#include <SoftwareSerial.h>
#include <Arduino.h>

PWMServo controller;
SoftwareSerial comm(6,5);

int demo = 0;

void setup() {
  // put your setup code here, to run once:
  controller.attach(SERVO_PIN_A);
  pinMode(13,OUTPUT);
  digitalWrite(13,LOW);
  comm.begin(9600);
  Serial.begin(9600);
}

void loop() {
  if(demo == 0){
    
  
  if(comm.available()){
    int val = comm.read();

      Serial.println(val);
      digitalWrite(13,HIGH);
//      delay(1000);
      controller.write(val);
  }
//  else{
//    digitalWrite(13,LOW);
//    controller.write(0);
//  }
//  Serial.println("nope");
  }

if(demo == 1){
  

int pos = 0;


  for(pos = 0; pos < 180; pos += 1) { // goes from 0 degrees to 180 degrees, 1 degree steps
    controller.write(pos);              // tell servo to go to position in variable 'pos'
    delay(15);                       // waits 15ms for the servo to reach the position
  }
  for(pos = 180; pos>=1; pos-=1) {   // goes from 180 degrees to 0 degrees
    controller.write(pos);              // tell servo to go to position in variable 'pos'
    delay(15);                       // waits 15ms for the servo to reach the position
  }
}
}
