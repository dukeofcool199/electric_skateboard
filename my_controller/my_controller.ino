#include <PWMServo.h>
#include <SoftwareSerial.h>
#include <Arduino.h>

PWMServo controller;
SoftwareSerial comm(6, 5);

int demo = 0;
int access = 0;

void setup() {
  // put your setup code here, to run once:
  controller.attach(SERVO_PIN_A);
  comm.begin(9600);
  Serial.begin(9600);
}

void loop() {
  if (access == 0){
    if(comm.available()){
      
    }
    return;
  }
  if (demo == 0) {
    if (comm.available()) {
      int val = comm.read();

      Serial.println(val);
      //delay(1000);
      controller.write(val);
    }
  }

  if (demo == 1) {
    int pos = 0;

    for (pos = 0; pos < 180; pos += 1) { // goes from 0 degrees to 180 degrees, 1 degree steps
      controller.write(pos);              // tell servo to go to position in variable 'pos'
      delay(15);                       // waits 15ms for the servo to reach the position
    }
    for (pos = 180; pos >= 1; pos -= 1) { // goes from 180 degrees to 0 degrees
      controller.write(pos);              // tell servo to go to position in variable 'pos'
      delay(15);                       // waits 15ms for the servo to reach the position
    }
  }
}
